import React from 'react';
import './App.css';
import PhoneInput from "./components/PhoneInput";
import ValidationTable from "./components/ValidationTable";
import PhoneStore from "./stores/PhoneStore";

import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

function App() {
    const store = new PhoneStore();

    return (
      <Layout className="layout">
          <Header>
              <div>Phone</div>
          </Header>
          <Content style={{ padding: '50px 50px', margin:"0 auto", width: "1600px" }}>
              <div className="App">
                  <PhoneInput store={store} />
                  <ValidationTable store={store} />
              </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>©2020 Created by Shanti</Footer>
      </Layout>
  );
}

export default App;
