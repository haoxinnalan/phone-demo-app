import {observable, computed, action} from 'mobx';
import PhoneValidationModel from '../models/PhoneValidationModel'

function uuid() {
    let uuid = '';
    for (let i = 0; i < 6; i++) {
        const random = Math.random() * 16 | 0;
        uuid += (i === 4 ? 4 : random).toString(16);
    }
    return uuid;
}

export default class PhoneValidationStore {
    @observable validations: Array<PhoneValidationModel> = [];

    @action
    addPhone (phone:string, result:object) {
        const exists = this.validations.filter(item=> item.phone===phone).length;
        if( exists > 0 )
            return false
        const phoneValidation = new PhoneValidationModel(this, uuid(), phone, result)
        this.validations.push(phoneValidation);
        return true
    }

    @computed get validPhoneCount() {
        return this.validations.filter(item => item.result).length;
    }

    checkPhoneExists(phone:string) {
        const exists = this.validations.filter(item=> item.phone===phone).length;
        return exists > 0
    }

    toJS() {
        return this.validations.map(item => item.toJS());
    }

}