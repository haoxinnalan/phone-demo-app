import {Button, Input, Form, Spin, message, Select} from "antd";
import React, { useState } from "react";
import { PhoneOutlined } from '@ant-design/icons';
import {observer} from 'mobx-react';
import axios from 'axios';

const { Option } = Select;

const PhoneInput = observer(({store}: any) => {

    // use form hook
    const [form] = Form.useForm();
    // use state hook
    // const [phone, setPhone] = useState("");
    const [loading, setLoading] = useState(false);

    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select style={{ width: 180 }}>
                <Option value="1">United States (+1)</Option>
                <Option value="86">China (+86)</Option>
                <Option value="852" selected>Hong Kong (+852)</Option>
            </Select>
        </Form.Item>
    );

    const addValidation = () => {
        let prefix = form.getFieldValue("prefix");
        let _phone = form.getFieldValue("phone");
        const phone = prefix + _phone;
        if(isNaN(phone)){
            form.resetFields();
            return message.error("Please enter digits only");
        }
        const isDup = store.checkPhoneExists(phone);
        if(isDup){
            return message.error("Duplicate phone number!");
        }
        setLoading(true);
        // axios
        axios.get("verify?phone=+"+phone)
            .then(response => {
                setLoading(false);
                const result = response.data;
                store.addPhone(phone, result);
                form.resetFields()
            }).catch(error=>console.log(error));
    }

    // const handleChange = (event: any) => {
    //     event.preventDefault();
    //     setPhone(event.target.value);
    //     console.debug("phone", phone);
    // }

    return(
        <div>
            <Spin spinning={loading}>
                <Form form={form} initialValues={{prefix: '852'}} layout="inline" name="control-ref">
                    <Form.Item
                        name="phone"
                        rules={[{ required: true, message: 'Please input phone!' }]}
                    >
                        <Input addonBefore={prefixSelector} placeholder="phone" size="large" prefix={<PhoneOutlined />} />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" style={{ marginLeft: 8 }} onClick={addValidation} size="large">
                            Validate
                        </Button>
                    </Form.Item>
                </Form>
            </Spin>
        </div>
    )
});

export default PhoneInput;